package timescale

import (
	"gitlab.com/thorchain/midgard/internal/common"
	"gitlab.com/thorchain/midgard/internal/models"
	"gitlab.com/thorchain/midgard/pkg/helpers"
	. "gopkg.in/check.v1"
)

func (s *TimeScaleSuite) TestGetTxDetailsByAddress(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	address, _ := common.NewAddress("bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	events, count, err := s.Store.GetTxDetails(address, common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeBnbEvent0.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(1))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(10))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(100))
	c.Assert(events[0].In.Memo, Equals, "stake:BNB.BNB")
	c.Assert(events[0].In.TxID, Equals, "2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	events, count, err = s.Store.GetTxDetails(address, common.EmptyTxID, common.EmptyAsset, nil, 0, 2)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeTomlEvent1.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(2))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(100))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(10))
	c.Assert(events[0].In.Memo, Equals, "stake:TOML")
	c.Assert(events[0].In.TxID, Equals, "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))
}

func (s *TimeScaleSuite) TestGetTxDetailsByAddressAsset(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	address, _ := common.NewAddress("bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	asset, _ := common.NewAsset("BNB")
	events, count, err := s.Store.GetTxDetails(address, common.EmptyTxID, asset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))

	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeBnbEvent0.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(1))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(10))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(100))
	c.Assert(events[0].In.Memo, Equals, "stake:BNB.BNB")
	c.Assert(events[0].In.TxID, Equals, "2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	address, _ = common.NewAddress("bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	asset, _ = common.NewAsset("BNB.TOML-4BC")
	events, count, err = s.Store.GetTxDetails(address, common.EmptyTxID, asset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeTomlEvent1.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(2))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(100))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(10))
	c.Assert(events[0].In.Memo, Equals, "stake:TOML")
	c.Assert(events[0].In.TxID, Equals, "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))
}

func (s *TimeScaleSuite) TestGetTxDetailsByAddressTxID(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	address, _ := common.NewAddress("bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	txid, _ := common.NewTxID("2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")
	events, count, err := s.Store.GetTxDetails(address, txid, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeBnbEvent0.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(1))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(10))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(100))
	c.Assert(events[0].In.Memo, Equals, "stake:BNB.BNB")
	c.Assert(events[0].In.TxID, Equals, "2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	txid, _ = common.NewTxID("E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
	events, count, err = s.Store.GetTxDetails(address, txid, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeTomlEvent1.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(2))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(100))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(10))
	c.Assert(events[0].In.Memo, Equals, "stake:TOML")
	c.Assert(events[0].In.TxID, Equals, "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))
}

func (s *TimeScaleSuite) TestGetTxDetailsByAsset(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	asset, _ := common.NewAsset("BNB")
	events, count, err := s.Store.GetTxDetails(common.NoAddress, common.EmptyTxID, asset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeBnbEvent0.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(1))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(10))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(100))
	c.Assert(events[0].In.Memo, Equals, "stake:BNB.BNB")
	c.Assert(events[0].In.TxID, Equals, "2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	asset, _ = common.NewAsset("BNB.TOML-4BC")
	events, count, err = s.Store.GetTxDetails(common.NoAddress, common.EmptyTxID, asset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0].Pool.Chain.String(), Equals, "BNB")
	c.Assert(events[0].Pool.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].Pool.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].Type, Equals, "stake")
	c.Assert(events[0].Status, Equals, "Success")
	c.Assert(events[0].Date, Equals, uint64(stakeTomlEvent1.Time.Unix()))
	c.Assert(events[0].Height, Equals, uint64(2))
	c.Assert(events[0].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(events[0].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[0].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(events[0].In.Coin[0].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(events[0].In.Coin[0].Amount, Equals, int64(100))
	c.Assert(events[0].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(events[0].In.Coin[1].Asset.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(events[0].In.Coin[1].Asset.Ticker.String(), Equals, "TOML")
	c.Assert(events[0].In.Coin[1].Amount, Equals, int64(10))
	c.Assert(events[0].In.Memo, Equals, "stake:TOML")
	c.Assert(events[0].In.TxID, Equals, "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
	c.Assert(len(events[0].Out), Equals, 0)
	c.Assert(events[0].Gas.Asset.Chain.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Symbol.IsEmpty(), Equals, true)
	c.Assert(events[0].Gas.Asset.Ticker.IsEmpty(), Equals, true)
	c.Assert(events[0].Options.WithdrawBasisPoints, Equals, float64(0))
	c.Assert(events[0].Options.PriceTarget, Equals, uint64(0))
	c.Assert(events[0].Options.Asymmetry, Equals, float64(0))
	c.Assert(events[0].Events.StakeUnits, Equals, int64(100))
	c.Assert(events[0].Events.Slip, Equals, float64(0))
	c.Assert(events[0].Events.Fee, Equals, uint64(0))
}

func (s *TimeScaleSuite) TestGetTxDetailsByEventType(c *C) {
	_, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(0))

	// Single stake
	err = s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)
	txDetail := models.TxDetails{
		Status: stakeBnbEvent0.Status,
		Type:   stakeBnbEvent0.Type,
		Height: uint64(stakeBnbEvent0.Height),
		Pool:   stakeBnbEvent0.Pool,
		In: models.TxData{
			Address:   stakeBnbEvent0.Event.InTx.FromAddress.String(),
			Coin:      stakeBnbEvent0.Event.InTx.Coins,
			Memo:      string(stakeBnbEvent0.InTx.Memo),
			TxID:      stakeBnbEvent0.InTx.ID.String(),
			Direction: "in",
			Pool:      "BNB.BNB",
			EventType: "stake",
		},
		Events: models.Events{
			StakeUnits: stakeBnbEvent0.StakeUnits,
		},
		Date: uint64(stakeBnbEvent0.Time.Unix()),
		Out:  make([]models.TxData, 0),
	}
	for _, tx := range stakeBnbEvent0.OutTxs {
		outTx := models.TxData{
			Address:   tx.FromAddress.String(),
			Coin:      tx.Coins,
			Memo:      string(tx.Memo),
			TxID:      tx.ID.String(),
			Direction: "out",
		}
		txDetail.Out = append(txDetail.Out, outTx)
	}
	evts := []models.TxDetails{
		txDetail,
	}

	events, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"stake"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	txDetail = models.TxDetails{
		Status: stakeTomlEvent1.Status,
		Type:   stakeTomlEvent1.Type,
		Height: uint64(stakeTomlEvent1.Height),
		Pool:   stakeTomlEvent1.Pool,
		In: models.TxData{
			Address:   stakeTomlEvent1.Event.InTx.FromAddress.String(),
			Coin:      stakeTomlEvent1.Event.InTx.Coins,
			Memo:      string(stakeTomlEvent1.InTx.Memo),
			TxID:      stakeTomlEvent1.InTx.ID.String(),
			Direction: "in",
			Pool:      "BNB.TOML-4BC",
			EventType: "stake",
		},
		Events: models.Events{
			StakeUnits: stakeTomlEvent1.StakeUnits,
		},
		Date: uint64(stakeTomlEvent1.Time.Unix()),
		Out:  make([]models.TxData, 0),
	}
	for _, tx := range stakeTomlEvent1.OutTxs {
		outTx := models.TxData{
			Address:   tx.FromAddress.String(),
			Coin:      tx.Coins,
			Memo:      string(tx.Memo),
			TxID:      tx.ID.String(),
			Direction: "out",
		}
		txDetail.Out = append(txDetail.Out, outTx)
	}
	evts = append(evts, txDetail)

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"stake"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(events[0], DeepEquals, evts[1])

	err = s.Store.CreateSwapRecord(&swapSellTusdb2RuneEvent0)
	c.Assert(err, IsNil)

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"stake"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(events[0], DeepEquals, evts[1])
}

func (s *TimeScaleSuite) TestGetTxDetailsAssetFilter(c *C) {
	_, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(0))
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)
	asset, _ := common.NewAsset("TOML-4BC")
	c.Assert(err, IsNil)
	events, count, err := s.Store.GetTxDetails("", common.EmptyTxID, asset, nil, 0, 50)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events, DeepEquals, []models.TxDetails{
		{
			Pool: asset,
			Type: "stake",
			In: models.TxData{
				Direction: "in",
				Pool:      "BNB.TOML-4BC",
				EventType: "stake",
				TxID:      "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998",
				Coin: common.Coins{
					common.Coin{
						Asset:  common.RuneB1AAsset,
						Amount: 100,
					},
					common.Coin{
						Asset:  asset,
						Amount: 10,
					},
				},
				Address: "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38",
				Memo:    "stake:TOML",
			},
			Height: 2,
			Date:   uint64(stakeTomlEvent1.Time.Unix()),
			Events: models.Events{
				StakeUnits: 100,
			},
			Status: "Success",
			Out:    []models.TxData{},
		},
	})

	err = s.Store.CreateUnStakesRecord(&unstakeTomlEvent0)
	c.Assert(err, IsNil)

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, asset, nil, 0, 50)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(len(events), Equals, 2)
	c.Assert(events, DeepEquals, []models.TxDetails{
		{
			Pool: asset,
			Type: "unstake",
			In: models.TxData{
				TxID: "24F5D0CF0DC1B1F1E3DA0DEC19E13252072F8E1F1CFB2839937C9DE38378E57C",
				Coin: common.Coins{
					common.Coin{
						Asset:  common.BNBAsset,
						Amount: 1,
					},
				},
				Address:   "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38",
				Memo:      "WITHDRAW:TOML-4BC",
				Direction: "in",
				Pool:      "BNB.TOML-4BC",
				EventType: "unstake",
			},
			Height: 3,
			Date:   uint64(unstakeTomlEvent0.Time.Unix()),
			Events: models.Events{
				StakeUnits: -100,
			},
			Status: "Success",
			Out: []models.TxData{
				{
					TxID: "4B074E4B83156A4E69A565B7E5AA8E106FC62F3390D9A947AA68BFEF2B092021",
					Coin: common.Coins{
						common.Coin{
							Asset:  asset,
							Amount: 10,
						},
					},
					Address:   "bnb1llvmhawaxxjchwmfmj8fjzftvwz4jpdhapp5hr",
					Memo:      "",
					Direction: "out",
				},
				{
					TxID: "E5869F3E93A4B0C0C63D79130ACBFA8A40590F0B54F82343E7F3C334C23F55B4",
					Coin: common.Coins{
						common.Coin{
							Asset:  common.RuneB1AAsset,
							Amount: 90,
						},
					},
					Address:   "bnb1llvmhawaxxjchwmfmj8fjzftvwz4jpdhapp5hr",
					Memo:      "",
					Direction: "out",
				},
			},
		},
		{
			Pool: asset,
			Type: "stake",
			In: models.TxData{
				TxID: "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998",
				Coin: common.Coins{
					common.Coin{
						Asset:  common.RuneB1AAsset,
						Amount: 100,
					},
					common.Coin{
						Asset:  asset,
						Amount: 10,
					},
				},
				Address:   "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38",
				Memo:      "stake:TOML",
				Direction: "in",
				Pool:      "BNB.TOML-4BC",
				EventType: "stake",
			},
			Height: 2,
			Date:   uint64(stakeTomlEvent1.Time.Unix()),
			Events: models.Events{
				StakeUnits: 100,
			},
			Status: "Success",
			Out:    []models.TxData{},
		},
	})

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.BNBAsset, nil, 0, 50)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(0))
	c.Assert(len(events), Equals, 0)
}

func (s *TimeScaleSuite) TestGetTxDetailsPagination(c *C) {
	_, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(0))

	// Single event
	err = s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	events, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(len(events), Equals, 1)

	// Additional event
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(len(events), Equals, 1)

	// Change page limit
	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 2)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(len(events), Equals, 2)

	// Change offset
	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 1, 2)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(len(events), Equals, 1)

	// Change offset
	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 2, 2)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(len(events), Equals, 0)
}

func (s *TimeScaleSuite) TestGetTxDetailsByDoubleSwap(c *C) {
	_, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(0))

	swapEvnt := swapBNB2Tusdb0
	swapEvnt.Type = "doubleSwap"
	err = s.Store.CreateSwapRecord(&swapEvnt)
	c.Assert(err, IsNil)
	swapEvnt = swapBNB2Tusdb1
	swapEvnt.Type = ""
	err = s.Store.CreateSwapRecord(&swapEvnt)
	c.Assert(err, IsNil)

	txDetail := models.TxDetails{
		Status: swapBNB2Tusdb0.Status,
		Type:   "doubleSwap",
		Height: uint64(swapBNB2Tusdb0.Height),
		Pool:   swapBNB2Tusdb0.Pool,
		In: models.TxData{
			Address:   swapBNB2Tusdb0.Event.InTx.FromAddress.String(),
			Coin:      swapBNB2Tusdb0.Event.InTx.Coins,
			Memo:      string(swapBNB2Tusdb0.InTx.Memo),
			TxID:      swapBNB2Tusdb0.InTx.ID.String(),
			Direction: "in",
			Pool:      "BNB.BNB",
			EventType: "doubleSwap",
		},
		Events: models.Events{
			Fee:  uint64(swapBNB2Tusdb0.LiquidityFee + swapBNB2Tusdb1.LiquidityFee),
			Slip: float64(swapBNB2Tusdb0.TradeSlip+swapBNB2Tusdb1.TradeSlip) / slipBasisPoints,
		},
		Date: uint64(swapBNB2Tusdb0.Time.Unix()),
		Out:  make([]models.TxData, 0),
	}
	for _, tx := range swapBNB2Tusdb1.OutTxs {
		outTx := models.TxData{
			Address:   tx.FromAddress.String(),
			Coin:      tx.Coins,
			Memo:      string(tx.Memo),
			TxID:      tx.ID.String(),
			Direction: "out",
		}
		txDetail.Out = append(txDetail.Out, outTx)
	}
	txDetail.Events.PriceTarget = 124958592
	txDetail.Options = models.Options{
		PriceTarget: 124958592,
	}
	evts := []models.TxDetails{
		txDetail,
	}

	events, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, nil, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"doubleSwap"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])

	err = s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)
	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"doubleSwap", "stake"}, 0, 2)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(2))
	c.Assert(events[1], DeepEquals, evts[0])

	err = s.Store.CreateSwapRecord(&swapBuyRune2BnbEvent3)
	c.Assert(err, IsNil)
	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"doubleSwap"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])

	txDetail = models.TxDetails{
		Status: swapBuyRune2BnbEvent3.Status,
		Type:   swapBuyRune2BnbEvent3.Type,
		Height: uint64(swapBuyRune2BnbEvent3.Height),
		Pool:   swapBuyRune2BnbEvent3.Pool,
		In: models.TxData{
			Address:   swapBuyRune2BnbEvent3.Event.InTx.FromAddress.String(),
			Coin:      swapBuyRune2BnbEvent3.Event.InTx.Coins,
			Memo:      string(swapBuyRune2BnbEvent3.InTx.Memo),
			TxID:      swapBuyRune2BnbEvent3.InTx.ID.String(),
			Direction: "in",
			Pool:      "BNB.BNB",
			EventType: "swap",
		},
		Events: models.Events{
			Fee:         uint64(swapBuyRune2BnbEvent3.LiquidityFee),
			Slip:        float64(swapBuyRune2BnbEvent3.TradeSlip) / slipBasisPoints,
			PriceTarget: 124958592,
		},
		Date: uint64(swapBuyRune2BnbEvent3.Time.Unix()),
		Out:  make([]models.TxData, 0),
		Options: models.Options{
			PriceTarget: 124958592,
		},
	}
	for _, tx := range swapBuyRune2BnbEvent3.OutTxs {
		outTx := models.TxData{
			Address:   tx.FromAddress.String(),
			Coin:      tx.Coins,
			Memo:      string(tx.Memo),
			TxID:      tx.ID.String(),
			Direction: "out",
		}
		txDetail.Out = append(txDetail.Out, outTx)
	}
	evts = []models.TxDetails{
		txDetail,
	}

	events, count, err = s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"swap"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])

	// Incomplete swap
	swapEvent := swapSellBnb2RuneEvent4
	swapEvent.OutTxs = nil
	err = s.Store.CreateSwapRecord(&swapEvent)
	c.Assert(err, IsNil)
	txDetail = models.TxDetails{
		Status: swapEvent.Status,
		Type:   swapEvent.Type,
		Height: uint64(swapEvent.Height),
		Pool:   swapEvent.Pool,
		In: models.TxData{
			Address:   swapEvent.Event.InTx.FromAddress.String(),
			Coin:      swapEvent.Event.InTx.Coins,
			Memo:      string(swapEvent.InTx.Memo),
			TxID:      swapEvent.InTx.ID.String(),
			Direction: "in",
			EventType: "swap",
			Pool:      "BNB.BNB",
		},
		Events: models.Events{
			Fee:         uint64(swapEvent.LiquidityFee),
			Slip:        float64(swapEvent.TradeSlip) / slipBasisPoints,
			PriceTarget: 124958592,
		},
		Date: uint64(swapEvent.Time.Unix()),
		Out:  make([]models.TxData, 0),
		Options: models.Options{
			PriceTarget: 124958592,
		},
	}
	evts = []models.TxDetails{
		txDetail,
	}
	events, count, err = s.Store.GetTxDetails("", swapEvent.InTx.ID, common.EmptyAsset, []string{"swap"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events[0], DeepEquals, evts[0])
}

func (s *TimeScaleSuite) TestGetEventsBasics(c *C) {
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)
	err = s.Store.CreateStakeRecord(&stakeBnbEvent1)
	c.Assert(err, IsNil)

	basics, err := s.Store.getEventsBasics([]int64{stakeBnbEvent0.ID, stakeBnbEvent1.ID})
	c.Assert(err, IsNil)

	c.Assert(basics[stakeBnbEvent0.ID], helpers.DeepEquals, models.EventBasic{
		Type:   stakeBnbEvent0.Type,
		Time:   stakeBnbEvent0.Time.UTC(),
		Height: uint64(stakeBnbEvent0.Height),
		Status: stakeBnbEvent0.Status,
	})

	c.Assert(basics[stakeBnbEvent1.ID], helpers.DeepEquals, models.EventBasic{
		Type:   stakeBnbEvent1.Type,
		Time:   stakeBnbEvent1.Time.UTC(),
		Height: uint64(stakeBnbEvent1.Height),
		Status: stakeBnbEvent1.Status,
	})
}

func (s *TimeScaleSuite) TestGetEvents(c *C) {
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)
	err = s.Store.CreateStakeRecord(&stakeBnbEvent1)
	c.Assert(err, IsNil)

	basics, err := s.Store.getEventsBasics([]int64{stakeBnbEvent0.ID, stakeBnbEvent1.ID})
	c.Assert(err, IsNil)

	events, err := s.Store.getEvents(basics)
	c.Assert(err, IsNil)

	c.Assert(events[stakeBnbEvent0.ID], helpers.DeepEquals, models.Events{
		StakeUnits: stakeBnbEvent0.StakeUnits,
	})

	c.Assert(events[stakeBnbEvent1.ID], helpers.DeepEquals, models.Events{
		StakeUnits: stakeBnbEvent1.StakeUnits,
	})
}

func (s *TimeScaleSuite) TestInTx(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	eventId := stakeBnbEvent0.ID
	inTx, err := s.Store.getEventsTxs([]int64{eventId})
	c.Assert(err, IsNil)

	c.Assert(inTx[eventId].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Symbol.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Ticker.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[0].Amount, Equals, int64(10))
	c.Assert(inTx[eventId].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[1].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(inTx[eventId].In.Coin[1].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(inTx[eventId].In.Coin[1].Amount, Equals, int64(100))
	c.Assert(inTx[eventId].In.Memo, Equals, "stake:BNB.BNB")
	c.Assert(inTx[eventId].In.TxID, Equals, "2F624637DE179665BA3322B864DB9F30001FD37B4E0D22A0B6ECE6A5B078DAB4")

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	eventId = stakeTomlEvent1.ID
	inTx, err = s.Store.getEventsTxs([]int64{eventId})
	c.Assert(err, IsNil)

	c.Assert(inTx[eventId].In.Address, Equals, "bnb1xlvns0n2mxh77mzaspn2hgav4rr4m8eerfju38")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Chain.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Symbol.String(), Equals, "RUNE-B1A")
	c.Assert(inTx[eventId].In.Coin[0].Asset.Ticker.String(), Equals, "RUNE")
	c.Assert(inTx[eventId].In.Coin[0].Amount, Equals, int64(100))
	c.Assert(inTx[eventId].In.Coin[1].Asset.Chain.String(), Equals, "BNB")
	c.Assert(inTx[eventId].In.Coin[1].Asset.Symbol.String(), Equals, "TOML-4BC")
	c.Assert(inTx[eventId].In.Coin[1].Asset.Ticker.String(), Equals, "TOML")
	c.Assert(inTx[eventId].In.Coin[1].Amount, Equals, int64(10))
	c.Assert(inTx[eventId].In.Memo, Equals, "stake:TOML")
	c.Assert(inTx[eventId].In.TxID, Equals, "E7A0395D6A013F37606B86FDDF17BB3B358217C2452B3F5C153E9A7D00FDA998")
}

func (s *TimeScaleSuite) TestOutTx(c *C) {
	// Single stake
	err := s.Store.CreateStakeRecord(&stakeBnbEvent0)
	c.Assert(err, IsNil)

	eventId := stakeBnbEvent0.ID
	outTxs, err := s.Store.getEventsTxs([]int64{eventId})
	c.Assert(err, IsNil)

	c.Assert(len(outTxs[eventId].Out), Equals, 0)

	// Additional stake
	err = s.Store.CreateStakeRecord(&stakeTomlEvent1)
	c.Assert(err, IsNil)

	eventId = stakeTomlEvent1.ID
	outTxs, err = s.Store.getEventsTxs([]int64{eventId})
	c.Assert(err, IsNil)

	c.Assert(len(outTxs[eventId].Out), Equals, 0)
}

func (s *TimeScaleSuite) TestRefundTx(c *C) {
	evt := &refundBOLTEvent0
	evt.Reason = "emit asset 3 less than price limit 1"
	err := s.Store.CreateRefundRecord(evt)
	c.Assert(err, IsNil)
	events, count, err := s.Store.GetTxDetails("", common.EmptyTxID, common.EmptyAsset, []string{"refund"}, 0, 1)
	c.Assert(err, IsNil)
	c.Assert(count, Equals, int64(1))
	c.Assert(events, DeepEquals, []models.TxDetails{
		{
			Pool: common.Asset{
				Chain:  "BNB",
				Symbol: "BOLT-014",
				Ticker: "BOLT",
			},
			Status: "Refund",
			Type:   "refund",
			In: models.TxData{
				Address: "bnb1asnv2dvsd64z25n6u5mh2838kmghq3a7876htr",
				TxID:    "416F961065DF50DC922D2DF18126A0D1917F4E4F05299CF42B0BC7DFB77A15F4",
				Coin: common.Coins{
					{
						Asset: common.Asset{
							Chain:  "BNB",
							Symbol: "BOLT-014",
							Ticker: "BLOT",
						},
						Amount: 10,
					},
					{
						Asset: common.Asset{
							Chain:  "BNB",
							Symbol: "RUNE-67C",
							Ticker: "RUNE",
						},
						Amount: 5,
					},
				},
				Direction: "in",
				Pool:      "BNB.BOLT-014",
				EventType: "refund",
			},
			Options: models.Options{
				Reason: "emit asset 3 less than price limit 1",
			},
			Out:    []models.TxData{},
			Date:   uint64(evt.Event.Time.Unix()),
			Height: 11,
			Events: models.Events{
				Reason: "emit asset 3 less than price limit 1",
			},
		},
	})
}
