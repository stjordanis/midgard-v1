package timescale

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"

	"github.com/huandu/go-sqlbuilder"
	"gitlab.com/thorchain/midgard/internal/common"
	"gitlab.com/thorchain/midgard/internal/models"
)

type eventTx struct {
	In  models.TxData
	Out []models.TxData
}

// GetTxDetails returns events with pagination and given query params.
func (s *Client) GetTxDetails(address common.Address, txID common.TxID, asset common.Asset, eventTypes []string, offset, limit int64) ([]models.TxDetails, int64, error) {
	txs, err := s.getTxDetails(address, txID, asset, eventTypes, offset, limit)
	if err != nil {
		return nil, 0, errors.Wrap(err, "GetTxDetails failed")
	}

	count, err := s.getTxsCount(address, txID, asset, eventTypes)
	if err != nil {
		return nil, 0, errors.Wrap(err, "GetTxDetails failed")
	}
	return txs, count, nil
}

func (s *Client) getTxDetails(address common.Address, txID common.TxID, asset common.Asset, eventTypes []string, offset, limit int64) ([]models.TxDetails, error) {
	q, args := s.buildEventsQuery(address.String(), txID.String(), asset.String(), eventTypes, false, limit, offset)
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "getTxDetails failed")
	}

	var events []int64
	for rows.Next() {
		results := make(map[string]interface{})
		err := rows.MapScan(results)
		if err != nil {
			return nil, errors.Wrap(err, "MapScan error")
		}

		eventID, _ := results["event_id"].(int64)
		events = append(events, eventID)
	}

	return s.processAllEvents(events)
}

func (s *Client) getTxsCount(address common.Address, txID common.TxID, asset common.Asset, eventTypes []string) (int64, error) {
	q, args := s.buildEventsQuery(address.String(), txID.String(), asset.String(), eventTypes, true, 0, 0)
	row := s.db.QueryRow(q, args...)

	var count sql.NullInt64
	if err := row.Scan(&count); err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
		return 0, errors.Wrap(err, "getTxsCount failed")
	}
	return count.Int64, nil
}

func (s *Client) buildEventsQuery(address, txID, asset string, eventTypes []string, isCount bool, limit, offset int64) (string, []interface{}) {
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	if isCount {
		sb.Select("COUNT(DISTINCT(txs.event_id))")
	} else {
		sb.Select("DISTINCT(txs.event_id)")
		sb.OrderBy("txs.event_id")
		sb.Desc()
		sb.Limit(int(limit))
		sb.Offset(int(offset))
	}
	sb.From("txs")
	if address != "" {
		sb.Where(sb.Or(sb.Equal("txs.from_address", address), sb.Equal("txs.to_address", address)))
	}
	if txID != "" {
		sb.Where(sb.Equal("txs.tx_hash", txID))
	}
	if asset != "." {
		sb.Where(sb.Equal("txs.pool", asset))
	}
	if len(eventTypes) > 0 {
		var types []interface{}
		for _, ev := range eventTypes {
			types = append(types, ev)
		}
		sb.Where(sb.In("txs.event_type", types...))
	}
	sb.Where("txs.event_type != ''")
	return sb.Build()
}

func (s *Client) processAllEvents(eventIds []int64) ([]models.TxDetails, error) {
	var txData []models.TxDetails
	basics, err := s.getEventsBasics(eventIds)
	if err != nil {
		return nil, errors.Wrap(err, "processEvents failed")
	}
	var doubleSwapIds []int64
	var allEventIds []int64
	for id, basic := range basics {
		if basic.Type == "doubleSwap" {
			if _, ok := basics[id+1]; !ok {
				doubleSwapIds = append(doubleSwapIds, id+1)
				allEventIds = append(allEventIds, id+1)
			}
		}
		allEventIds = append(allEventIds, id)
	}

	doubleSwapBasics, err := s.getEventsBasics(doubleSwapIds)
	if err != nil {
		return nil, errors.Wrap(err, "processEvents failed")
	}
	for k, v := range doubleSwapBasics {
		basics[k] = v
	}
	eventTxs, err := s.getEventsTxs(allEventIds)
	if err != nil {
		return nil, errors.Wrap(err, "processEvents failed")
	}
	events, err := s.getEvents(basics)
	if err != nil {
		return nil, errors.Wrap(err, "processEvents failed")
	}
	var outTx []models.TxData
	for _, eventId := range eventIds {
		eventBasic := basics[eventId]
		var event1 models.Events
		inTx := eventTxs[eventId].In
		outTx = eventTxs[eventId].Out
		if eventBasic.Type == "doubleSwap" {
			event1 = events[eventId]
			outTx = eventTxs[eventId+1].Out
			event2 := events[eventId+1]
			event1.Slip += event2.Slip
			event1.Fee += event2.Fee
			if len(outTx) == 0 {
				eventBasic.Status = "pending"
			}
		} else {
			event1 = events[eventId]
		}
		option := models.Options{
			PriceTarget: events[eventId].PriceTarget,
			Reason:      events[eventId].Reason,
		}
		if outTx == nil {
			outTx = make([]models.TxData, 0)
		}
		pool, _ := common.NewAsset(inTx.Pool)
		if pool.IsEmpty() {
			for _, tx := range outTx {
				pool, _ = common.NewAsset(tx.Pool)
				if !pool.IsEmpty() {
					break
				}
			}
		}
		txData = append(txData, models.TxDetails{
			Pool:    pool,
			Type:    eventBasic.Type,
			Status:  eventBasic.Status,
			In:      inTx,
			Out:     outTx,
			Options: option,
			Events:  event1,
			Date:    uint64(eventBasic.Time.Unix()),
			Height:  eventBasic.Height,
		})
	}
	return txData, nil
}

func (s *Client) gas(eventId uint64) models.TxGas {
	stmnt := `
		SELECT gas.pool, gas.amount
			FROM gas
		WHERE event_id = $1;`

	var (
		pool   string
		amount uint64
	)

	row := s.db.QueryRow(stmnt, eventId)
	if err := row.Scan(&pool, &amount); err != nil {
		return models.TxGas{}
	}

	asset, _ := common.NewAsset(pool)
	return models.TxGas{
		Asset:  asset,
		Amount: amount,
	}
}

func (s *Client) getEvents(basics map[int64]models.EventBasic) (map[int64]models.Events, error) {
	var swapEventIds, stakeEventIds, refundEventIds []int64
	for eventId, basic := range basics {
		if basic.Type == "refund" {
			refundEventIds = append(refundEventIds, eventId)
		} else if basic.Type == "stake" || basic.Type == "unstake" {
			stakeEventIds = append(stakeEventIds, eventId)
		} else {
			swapEventIds = append(swapEventIds, eventId)
		}
	}
	swapEvents, err := s.swapsEvents(swapEventIds)
	if err != nil {
		return nil, errors.Wrap(err, "getEvents failed")
	}
	allEvents := swapEvents
	stakeEvents, err := s.stakesEvents(stakeEventIds)
	if err != nil {
		return nil, errors.Wrap(err, "getEvents failed")
	}
	for k, v := range stakeEvents {
		allEvents[k] = v
	}
	refundEvent, err := s.getRefundReasons(refundEventIds)
	if err != nil {
		return nil, errors.Wrap(err, "getEvents failed")
	}
	for k, v := range refundEvent {
		allEvents[k] = v
	}
	return allEvents, nil
}

func (s *Client) swapsEvents(eventsId []int64) (map[int64]models.Events, error) {
	if len(eventsId) == 0 {
		return map[int64]models.Events{}, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("event_id,trade_slip, liquidity_fee,price_target")
	sb.From("swaps")
	var ids []interface{}
	for _, eventId := range eventsId {
		ids = append(ids, eventId)
	}
	sb.Where(sb.In("event_id", ids...))
	q, args := sb.Build()
	events := make(map[int64]models.Events)
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "swapsEvents failed")
	}
	var (
		eventId       int64
		liquidity_fee uint64
		trade_slip    float64
		price_target  uint64
	)
	for rows.Next() {
		if err := rows.Scan(&eventId, &trade_slip, &liquidity_fee, &price_target); err != nil {
			return nil, errors.Wrap(err, "swapsEvents failed")
		}
		events[eventId] = models.Events{
			Fee:         liquidity_fee,
			Slip:        trade_slip,
			PriceTarget: price_target,
		}
	}
	return events, nil
}

func (s *Client) stakesEvents(eventsId []int64) (map[int64]models.Events, error) {
	if len(eventsId) == 0 {
		return map[int64]models.Events{}, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("event_id,units")
	sb.From("pools_history")
	var ids []interface{}
	for _, eventId := range eventsId {
		ids = append(ids, eventId)
	}
	sb.Where(sb.In("event_id", ids...))
	sb.Where("units!=0")
	q, args := sb.Build()
	events := make(map[int64]models.Events)
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "stakesEvents failed")
	}
	var (
		eventId int64
		units   int64
	)
	for rows.Next() {
		if err := rows.Scan(&eventId, &units); err != nil {
			return nil, errors.Wrap(err, "stakesEvents failed")
		}
		events[eventId] = models.Events{
			StakeUnits: units,
		}
	}
	return events, nil
}

func (s *Client) getRefundReasons(eventsId []int64) (map[int64]models.Events, error) {
	if len(eventsId) == 0 {
		return map[int64]models.Events{}, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("event_id, meta->>'reason'")
	sb.From("pools_history")
	var ids []interface{}
	for _, eventId := range eventsId {
		ids = append(ids, eventId)
	}
	sb.Where(sb.In("event_id", ids...))
	q, args := sb.Build()
	events := make(map[int64]models.Events)
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "getRefundReasons failed")
	}
	var eventId int64
	var reason sql.NullString
	for rows.Next() {
		if err := rows.Scan(&eventId, &reason); err != nil {
			return nil, errors.Wrap(err, "getRefundReasons failed")
		}
		if reason.Valid {
			events[eventId] = models.Events{
				Reason: reason.String,
			}
		}
	}
	return events, nil
}

func (s *Client) getEventBasics(eventIds []uint64) ([]models.EventBasic, error) {
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("id,time, height, type, status")
	sb.From("events")
	var ids []interface{}
	for _, ev := range eventIds {
		ids = append(ids, ev)
	}
	sb.Where(sb.In("id", ids...))
	rows, err := s.db.Queryx(sb.Build())
	if err != nil {
		return nil, errors.Wrap(err, "getEventBasics failed")
	}

	var basics []models.EventBasic
	var (
		eventId   int64
		eventTime time.Time
		height    uint64
		eventType string
		status    string
	)
	for rows.Next() {
		if err := rows.Scan(&eventId, &eventTime, &height, &eventType, &status); err != nil {
			return nil, errors.Wrap(err, "getEventBasics failed")
		}
		basics = append(basics, models.EventBasic{
			Type:   eventType,
			Height: height,
			Time:   eventTime,
			Status: status,
		})
	}
	return basics, nil
}

func (s *Client) getEventsBasics(eventIds []int64) (map[int64]models.EventBasic, error) {
	if len(eventIds) == 0 {
		return map[int64]models.EventBasic{}, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("id,time, height, type, status")
	sb.From("events")
	var ids []interface{}
	for _, ev := range eventIds {
		ids = append(ids, ev)
	}
	sb.Where(sb.In("id", ids...))
	q, args := sb.Build()
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "getEventsBasics failed")
	}

	basics := make(map[int64]models.EventBasic)
	var (
		eventId   int64
		eventTime time.Time
		height    uint64
		eventType string
		status    string
	)
	for rows.Next() {
		if err := rows.Scan(&eventId, &eventTime, &height, &eventType, &status); err != nil {
			return nil, errors.Wrap(err, "getEventsBasics failed")
		}
		basics[eventId] = models.EventBasic{
			Type:   eventType,
			Height: height,
			Time:   eventTime,
			Status: status,
		}
	}
	return basics, nil
}

func (s *Client) getEventsTxs(eventIds []int64) (map[int64]eventTx, error) {
	if len(eventIds) == 0 {
		return map[int64]eventTx{}, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("tx_hash,event_id, memo, from_address,direction,pool,event_type")
	sb.From("txs")
	var ids []interface{}
	for _, ev := range eventIds {
		ids = append(ids, ev)
	}
	sb.Where(sb.In("event_id", ids...))
	sb.OrderBy("tx_hash")
	q, args := sb.Build()
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "getEventsTxs failed")
	}

	var (
		txID        string
		eventId     int64
		memo        string
		fromAddress string
		direction   string
		pool        string
		eventType   string
	)
	txData := make(map[int64][]models.TxData)
	for rows.Next() {
		if err := rows.Scan(&txID, &eventId, &memo, &fromAddress, &direction, &pool, &eventType); err != nil {
			return nil, errors.Wrap(err, "getEventsTxs failed")
		}
		txData[eventId] = append(txData[eventId], models.TxData{
			TxID:      txID,
			Memo:      memo,
			Address:   fromAddress,
			Direction: direction,
			Pool:      pool,
			EventType: eventType,
		})
	}
	txData, err = s.getTxsCoins(txData)
	if err != nil {
		return nil, errors.Wrap(err, "getEventsTxs failed")
	}
	eventTxs := make(map[int64]eventTx)
	for eventId, txs := range txData {
		for _, tx := range txs {
			if tx.Direction == "in" {
				eventTxs[eventId] = eventTx{
					In:  tx,
					Out: eventTxs[eventId].Out,
				}
			} else {
				eventTxs[eventId] = eventTx{
					In:  eventTxs[eventId].In,
					Out: append(eventTxs[eventId].Out, tx),
				}
			}
		}
	}
	return eventTxs, nil
}

func (s *Client) getTxsCoins(eventTxs map[int64][]models.TxData) (map[int64][]models.TxData, error) {
	if len(eventTxs) == 0 {
		return eventTxs, nil
	}
	sb := sqlbuilder.PostgreSQL.NewSelectBuilder()
	sb.Select("tx_hash,chain, symbol, ticker, amount,event_id")
	sb.From("coins")
	var ids []interface{}
	for _, txs := range eventTxs {
		for _, tx := range txs {
			ids = append(ids, tx.TxID)
		}
	}
	sb.Where(sb.In("coins.tx_hash", ids...))
	sb.OrderBy("symbol")
	q, args := sb.Build()
	rows, err := s.db.Queryx(q, args...)
	if err != nil {
		return nil, errors.Wrap(err, "getTxsCoins failed")
	}
	for rows.Next() {
		results := make(map[string]interface{})
		err = rows.MapScan(results)
		if err != nil {
			s.logger.Err(err).Msg("MapScan error")
			continue
		}

		ch, _ := results["chain"].(string)
		chain, _ := common.NewChain(ch)

		sym, _ := results["symbol"].(string)
		symbol, _ := common.NewSymbol(sym)

		t, _ := results["ticker"].(string)
		ticker, _ := common.NewTicker(t)

		txHash, _ := results["tx_hash"].(string)

		event_id, _ := results["event_id"].(int64)

		coin := common.Coin{
			Asset: common.Asset{
				Chain:  chain,
				Symbol: symbol,
				Ticker: ticker,
			},
			Amount: results["amount"].(int64),
		}
		for idx, tx := range eventTxs[event_id] {
			if tx.TxID == txHash {
				if len(eventTxs[event_id][idx].Coin) < 2 {
					eventTxs[event_id][idx].Coin = append(tx.Coin, coin)
				}
			}
		}
	}
	return eventTxs, nil
}
